// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HDKEditorUtilities/Public/WorldOutliner/HDKEdWorldOutlinerSubsystem.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHDKEdWorldOutlinerSubsystem() {}
// Cross Module References
	HDKEDITORUTILITIES_API UClass* Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_NoRegister();
	HDKEDITORUTILITIES_API UClass* Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem();
	EDITORSUBSYSTEM_API UClass* Z_Construct_UClass_UEditorSubsystem();
	UPackage* Z_Construct_UPackage__Script_HDKEditorUtilities();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
// End Cross Module References
	DEFINE_FUNCTION(UHDKEdWorldOutlinerSubsystem::execUpdateActors)
	{
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_Actors);
		P_GET_UBOOL(Z_Param_bCutPrefixes);
		P_GET_UBOOL(Z_Param_bCutSuffixes);
		P_GET_UBOOL(Z_Param_bAddIndexes);
		P_GET_UBOOL(Z_Param_bCreateFolders);
		P_GET_UBOOL(Z_Param_bExcludeFolders);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->UpdateActors(Z_Param_Out_Actors,Z_Param_bCutPrefixes,Z_Param_bCutSuffixes,Z_Param_bAddIndexes,Z_Param_bCreateFolders,Z_Param_bExcludeFolders);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UHDKEdWorldOutlinerSubsystem::execUpdateActor)
	{
		P_GET_OBJECT(AActor,Z_Param_Actor);
		P_GET_UBOOL(Z_Param_bCutPrefixes);
		P_GET_UBOOL(Z_Param_bCutSuffixes);
		P_GET_UBOOL(Z_Param_bAddIndex);
		P_GET_UBOOL(Z_Param_bCreateFolders);
		P_GET_UBOOL(Z_Param_bExcludeFolders);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->UpdateActor(Z_Param_Actor,Z_Param_bCutPrefixes,Z_Param_bCutSuffixes,Z_Param_bAddIndex,Z_Param_bCreateFolders,Z_Param_bExcludeFolders);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UHDKEdWorldOutlinerSubsystem::execOnNewActorsDropped)
	{
		P_GET_TARRAY_REF(UObject*,Z_Param_Out_Objects);
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_Actors);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnNewActorsDropped(Z_Param_Out_Objects,Z_Param_Out_Actors);
		P_NATIVE_END;
	}
	void UHDKEdWorldOutlinerSubsystem::StaticRegisterNativesUHDKEdWorldOutlinerSubsystem()
	{
		UClass* Class = UHDKEdWorldOutlinerSubsystem::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnNewActorsDropped", &UHDKEdWorldOutlinerSubsystem::execOnNewActorsDropped },
			{ "UpdateActor", &UHDKEdWorldOutlinerSubsystem::execUpdateActor },
			{ "UpdateActors", &UHDKEdWorldOutlinerSubsystem::execUpdateActors },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics
	{
		struct HDKEdWorldOutlinerSubsystem_eventOnNewActorsDropped_Parms
		{
			TArray<UObject*> Objects;
			TArray<AActor*> Actors;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Objects_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Objects_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Objects;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Actors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Actors;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics::NewProp_Objects_Inner = { "Objects", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics::NewProp_Objects_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics::NewProp_Objects = { "Objects", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HDKEdWorldOutlinerSubsystem_eventOnNewActorsDropped_Parms, Objects), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics::NewProp_Objects_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics::NewProp_Objects_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics::NewProp_Actors_Inner = { "Actors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics::NewProp_Actors_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics::NewProp_Actors = { "Actors", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HDKEdWorldOutlinerSubsystem_eventOnNewActorsDropped_Parms, Actors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics::NewProp_Actors_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics::NewProp_Actors_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics::NewProp_Objects_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics::NewProp_Objects,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics::NewProp_Actors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics::NewProp_Actors,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09* Event called when new actors being dropped to the level\n\x09* @param Objects - Referenced assets\n\x09* @param Actors\x09- Dropped actors\n\x09*/" },
		{ "ModuleRelativePath", "Public/WorldOutliner/HDKEdWorldOutlinerSubsystem.h" },
		{ "ToolTip", "Event called when new actors being dropped to the level\n@param Objects - Referenced assets\n@param Actors - Dropped actors" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem, nullptr, "OnNewActorsDropped", nullptr, nullptr, sizeof(HDKEdWorldOutlinerSubsystem_eventOnNewActorsDropped_Parms), Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00480401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics
	{
		struct HDKEdWorldOutlinerSubsystem_eventUpdateActor_Parms
		{
			AActor* Actor;
			bool bCutPrefixes;
			bool bCutSuffixes;
			bool bAddIndex;
			bool bCreateFolders;
			bool bExcludeFolders;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actor;
		static void NewProp_bCutPrefixes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCutPrefixes;
		static void NewProp_bCutSuffixes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCutSuffixes;
		static void NewProp_bAddIndex_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAddIndex;
		static void NewProp_bCreateFolders_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCreateFolders;
		static void NewProp_bExcludeFolders_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bExcludeFolders;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::NewProp_Actor = { "Actor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HDKEdWorldOutlinerSubsystem_eventUpdateActor_Parms, Actor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::NewProp_bCutPrefixes_SetBit(void* Obj)
	{
		((HDKEdWorldOutlinerSubsystem_eventUpdateActor_Parms*)Obj)->bCutPrefixes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::NewProp_bCutPrefixes = { "bCutPrefixes", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(HDKEdWorldOutlinerSubsystem_eventUpdateActor_Parms), &Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::NewProp_bCutPrefixes_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::NewProp_bCutSuffixes_SetBit(void* Obj)
	{
		((HDKEdWorldOutlinerSubsystem_eventUpdateActor_Parms*)Obj)->bCutSuffixes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::NewProp_bCutSuffixes = { "bCutSuffixes", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(HDKEdWorldOutlinerSubsystem_eventUpdateActor_Parms), &Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::NewProp_bCutSuffixes_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::NewProp_bAddIndex_SetBit(void* Obj)
	{
		((HDKEdWorldOutlinerSubsystem_eventUpdateActor_Parms*)Obj)->bAddIndex = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::NewProp_bAddIndex = { "bAddIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(HDKEdWorldOutlinerSubsystem_eventUpdateActor_Parms), &Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::NewProp_bAddIndex_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::NewProp_bCreateFolders_SetBit(void* Obj)
	{
		((HDKEdWorldOutlinerSubsystem_eventUpdateActor_Parms*)Obj)->bCreateFolders = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::NewProp_bCreateFolders = { "bCreateFolders", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(HDKEdWorldOutlinerSubsystem_eventUpdateActor_Parms), &Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::NewProp_bCreateFolders_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::NewProp_bExcludeFolders_SetBit(void* Obj)
	{
		((HDKEdWorldOutlinerSubsystem_eventUpdateActor_Parms*)Obj)->bExcludeFolders = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::NewProp_bExcludeFolders = { "bExcludeFolders", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(HDKEdWorldOutlinerSubsystem_eventUpdateActor_Parms), &Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::NewProp_bExcludeFolders_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::NewProp_Actor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::NewProp_bCutPrefixes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::NewProp_bCutSuffixes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::NewProp_bAddIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::NewProp_bCreateFolders,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::NewProp_bExcludeFolders,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::Function_MetaDataParams[] = {
		{ "Category", "HDK Editor Utilities|World Outliner Subsystem" },
		{ "Comment", "/**\n\x09* Update actor naming and folder path.\n\x09* Actor being named according to their destination and being put to folders according to their location.\n\x09* @param Actor - Actor to organize\n\x09* @param Rename - If true, actor will be renamed\n\x09* @param Index - If true, add index to actor name\n\x09* @param CreateFolders - If true, actor will be put to folders according to their location\n\x09* @param ExcludeFolders - If true, exclude folders from actors path\n\x09*/" },
		{ "CPP_Default_bAddIndex", "false" },
		{ "CPP_Default_bCreateFolders", "true" },
		{ "CPP_Default_bCutPrefixes", "true" },
		{ "CPP_Default_bCutSuffixes", "true" },
		{ "CPP_Default_bExcludeFolders", "true" },
		{ "ModuleRelativePath", "Public/WorldOutliner/HDKEdWorldOutlinerSubsystem.h" },
		{ "ToolTip", "Update actor naming and folder path.\nActor being named according to their destination and being put to folders according to their location.\n@param Actor - Actor to organize\n@param Rename - If true, actor will be renamed\n@param Index - If true, add index to actor name\n@param CreateFolders - If true, actor will be put to folders according to their location\n@param ExcludeFolders - If true, exclude folders from actors path" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem, nullptr, "UpdateActor", nullptr, nullptr, sizeof(HDKEdWorldOutlinerSubsystem_eventUpdateActor_Parms), Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics
	{
		struct HDKEdWorldOutlinerSubsystem_eventUpdateActors_Parms
		{
			TArray<AActor*> Actors;
			bool bCutPrefixes;
			bool bCutSuffixes;
			bool bAddIndexes;
			bool bCreateFolders;
			bool bExcludeFolders;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Actors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Actors;
		static void NewProp_bCutPrefixes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCutPrefixes;
		static void NewProp_bCutSuffixes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCutSuffixes;
		static void NewProp_bAddIndexes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAddIndexes;
		static void NewProp_bCreateFolders_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCreateFolders;
		static void NewProp_bExcludeFolders_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bExcludeFolders;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_Actors_Inner = { "Actors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_Actors_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_Actors = { "Actors", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(HDKEdWorldOutlinerSubsystem_eventUpdateActors_Parms, Actors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_Actors_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_Actors_MetaData)) };
	void Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_bCutPrefixes_SetBit(void* Obj)
	{
		((HDKEdWorldOutlinerSubsystem_eventUpdateActors_Parms*)Obj)->bCutPrefixes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_bCutPrefixes = { "bCutPrefixes", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(HDKEdWorldOutlinerSubsystem_eventUpdateActors_Parms), &Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_bCutPrefixes_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_bCutSuffixes_SetBit(void* Obj)
	{
		((HDKEdWorldOutlinerSubsystem_eventUpdateActors_Parms*)Obj)->bCutSuffixes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_bCutSuffixes = { "bCutSuffixes", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(HDKEdWorldOutlinerSubsystem_eventUpdateActors_Parms), &Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_bCutSuffixes_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_bAddIndexes_SetBit(void* Obj)
	{
		((HDKEdWorldOutlinerSubsystem_eventUpdateActors_Parms*)Obj)->bAddIndexes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_bAddIndexes = { "bAddIndexes", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(HDKEdWorldOutlinerSubsystem_eventUpdateActors_Parms), &Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_bAddIndexes_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_bCreateFolders_SetBit(void* Obj)
	{
		((HDKEdWorldOutlinerSubsystem_eventUpdateActors_Parms*)Obj)->bCreateFolders = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_bCreateFolders = { "bCreateFolders", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(HDKEdWorldOutlinerSubsystem_eventUpdateActors_Parms), &Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_bCreateFolders_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_bExcludeFolders_SetBit(void* Obj)
	{
		((HDKEdWorldOutlinerSubsystem_eventUpdateActors_Parms*)Obj)->bExcludeFolders = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_bExcludeFolders = { "bExcludeFolders", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(HDKEdWorldOutlinerSubsystem_eventUpdateActors_Parms), &Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_bExcludeFolders_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_Actors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_Actors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_bCutPrefixes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_bCutSuffixes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_bAddIndexes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_bCreateFolders,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::NewProp_bExcludeFolders,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::Function_MetaDataParams[] = {
		{ "Category", "HDK Editor Utilities|World Outliner Subsystem" },
		{ "Comment", "/**\n\x09* Update actors naming and folder path.\n\x09* Actors being named according to their destination and being put to folders according to their location.\n\x09* @param Actors - Actors to organize\n\x09* @param Rename - If true, actors will be renamed\n\x09* @param Index - If true, add index to actors name\n\x09* @param CreateFolders - If true, actors will be put to folders according to their location\n\x09* @param ExcludeFolders - If true, collapse folders from actors path\n\x09*/" },
		{ "CPP_Default_bAddIndexes", "false" },
		{ "CPP_Default_bCreateFolders", "true" },
		{ "CPP_Default_bCutPrefixes", "true" },
		{ "CPP_Default_bCutSuffixes", "true" },
		{ "CPP_Default_bExcludeFolders", "true" },
		{ "ModuleRelativePath", "Public/WorldOutliner/HDKEdWorldOutlinerSubsystem.h" },
		{ "ToolTip", "Update actors naming and folder path.\nActors being named according to their destination and being put to folders according to their location.\n@param Actors - Actors to organize\n@param Rename - If true, actors will be renamed\n@param Index - If true, add index to actors name\n@param CreateFolders - If true, actors will be put to folders according to their location\n@param ExcludeFolders - If true, collapse folders from actors path" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem, nullptr, "UpdateActors", nullptr, nullptr, sizeof(HDKEdWorldOutlinerSubsystem_eventUpdateActors_Parms), Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_NoRegister()
	{
		return UHDKEdWorldOutlinerSubsystem::StaticClass();
	}
	struct Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PrefixesToCut_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrefixesToCut_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PrefixesToCut;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoCutPrefixes_MetaData[];
#endif
		static void NewProp_bAutoCutPrefixes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoCutPrefixes;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SuffixesToCut_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SuffixesToCut_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SuffixesToCut;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoCutSuffixes_MetaData[];
#endif
		static void NewProp_bAutoCutSuffixes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoCutSuffixes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoAddIndexes_MetaData[];
#endif
		static void NewProp_bAutoAddIndexes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoAddIndexes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoCreateFolders_MetaData[];
#endif
		static void NewProp_bAutoCreateFolders_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoCreateFolders;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PathOverridesByClass_ValueProp;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_PathOverridesByClass_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PathOverridesByClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_PathOverridesByClass;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FoldersToExclude_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FoldersToExclude_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FoldersToExclude;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoExcludeFolders_MetaData[];
#endif
		static void NewProp_bAutoExcludeFolders_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoExcludeFolders;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEditorSubsystem,
		(UObject* (*)())Z_Construct_UPackage__Script_HDKEditorUtilities,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_OnNewActorsDropped, "OnNewActorsDropped" }, // 1161273678
		{ &Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActor, "UpdateActor" }, // 3921790994
		{ &Z_Construct_UFunction_UHDKEdWorldOutlinerSubsystem_UpdateActors, "UpdateActors" }, // 2742968425
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Editor subsystem, that correctly rename actors which developer dropped to level and move to paths */" },
		{ "DisplayName", "World Outliner Subsystem" },
		{ "IncludePath", "WorldOutliner/HDKEdWorldOutlinerSubsystem.h" },
		{ "ModuleRelativePath", "Public/WorldOutliner/HDKEdWorldOutlinerSubsystem.h" },
		{ "ToolTip", "Editor subsystem, that correctly rename actors which developer dropped to level and move to paths" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_PrefixesToCut_Inner = { "PrefixesToCut", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_PrefixesToCut_MetaData[] = {
		{ "Category", "Naming" },
		{ "Comment", "/** Prefixes to cut form actors label */" },
		{ "ModuleRelativePath", "Public/WorldOutliner/HDKEdWorldOutlinerSubsystem.h" },
		{ "ToolTip", "Prefixes to cut form actors label" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_PrefixesToCut = { "PrefixesToCut", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHDKEdWorldOutlinerSubsystem, PrefixesToCut), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_PrefixesToCut_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_PrefixesToCut_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoCutPrefixes_MetaData[] = {
		{ "Category", "Naming" },
		{ "Comment", "/** If true, auto cut specified prefixes from actors label on drop to the viewport */" },
		{ "ModuleRelativePath", "Public/WorldOutliner/HDKEdWorldOutlinerSubsystem.h" },
		{ "ToolTip", "If true, auto cut specified prefixes from actors label on drop to the viewport" },
	};
#endif
	void Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoCutPrefixes_SetBit(void* Obj)
	{
		((UHDKEdWorldOutlinerSubsystem*)Obj)->bAutoCutPrefixes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoCutPrefixes = { "bAutoCutPrefixes", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UHDKEdWorldOutlinerSubsystem), &Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoCutPrefixes_SetBit, METADATA_PARAMS(Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoCutPrefixes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoCutPrefixes_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_SuffixesToCut_Inner = { "SuffixesToCut", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_SuffixesToCut_MetaData[] = {
		{ "Category", "Naming" },
		{ "Comment", "/** Suffixes to cut form actors label */" },
		{ "ModuleRelativePath", "Public/WorldOutliner/HDKEdWorldOutlinerSubsystem.h" },
		{ "ToolTip", "Suffixes to cut form actors label" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_SuffixesToCut = { "SuffixesToCut", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHDKEdWorldOutlinerSubsystem, SuffixesToCut), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_SuffixesToCut_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_SuffixesToCut_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoCutSuffixes_MetaData[] = {
		{ "Category", "Naming" },
		{ "Comment", "/** If true, auto cut specified suffixes from actors label on drop to the viewport */" },
		{ "ModuleRelativePath", "Public/WorldOutliner/HDKEdWorldOutlinerSubsystem.h" },
		{ "ToolTip", "If true, auto cut specified suffixes from actors label on drop to the viewport" },
	};
#endif
	void Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoCutSuffixes_SetBit(void* Obj)
	{
		((UHDKEdWorldOutlinerSubsystem*)Obj)->bAutoCutSuffixes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoCutSuffixes = { "bAutoCutSuffixes", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UHDKEdWorldOutlinerSubsystem), &Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoCutSuffixes_SetBit, METADATA_PARAMS(Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoCutSuffixes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoCutSuffixes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoAddIndexes_MetaData[] = {
		{ "Category", "Naming" },
		{ "Comment", "/** If true, auto add indexes to actors label on drop to the viewport */" },
		{ "ModuleRelativePath", "Public/WorldOutliner/HDKEdWorldOutlinerSubsystem.h" },
		{ "ToolTip", "If true, auto add indexes to actors label on drop to the viewport" },
	};
#endif
	void Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoAddIndexes_SetBit(void* Obj)
	{
		((UHDKEdWorldOutlinerSubsystem*)Obj)->bAutoAddIndexes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoAddIndexes = { "bAutoAddIndexes", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UHDKEdWorldOutlinerSubsystem), &Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoAddIndexes_SetBit, METADATA_PARAMS(Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoAddIndexes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoAddIndexes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoCreateFolders_MetaData[] = {
		{ "Category", "Folders" },
		{ "Comment", "/** If true, on actor's dropped to the viewport auto create world outliner folder hierarchy from it's path or path override by class and place actor to that folder */" },
		{ "ModuleRelativePath", "Public/WorldOutliner/HDKEdWorldOutlinerSubsystem.h" },
		{ "ToolTip", "If true, on actor's dropped to the viewport auto create world outliner folder hierarchy from it's path or path override by class and place actor to that folder" },
	};
#endif
	void Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoCreateFolders_SetBit(void* Obj)
	{
		((UHDKEdWorldOutlinerSubsystem*)Obj)->bAutoCreateFolders = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoCreateFolders = { "bAutoCreateFolders", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UHDKEdWorldOutlinerSubsystem), &Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoCreateFolders_SetBit, METADATA_PARAMS(Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoCreateFolders_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoCreateFolders_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_PathOverridesByClass_ValueProp = { "PathOverridesByClass", nullptr, (EPropertyFlags)0x0000000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_PathOverridesByClass_Key_KeyProp = { "PathOverridesByClass_Key", nullptr, (EPropertyFlags)0x0000000000004001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_PathOverridesByClass_MetaData[] = {
		{ "Category", "Folders" },
		{ "Comment", "/** Override paths for actors by their class */" },
		{ "ModuleRelativePath", "Public/WorldOutliner/HDKEdWorldOutlinerSubsystem.h" },
		{ "ToolTip", "Override paths for actors by their class" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_PathOverridesByClass = { "PathOverridesByClass", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHDKEdWorldOutlinerSubsystem, PathOverridesByClass), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_PathOverridesByClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_PathOverridesByClass_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_FoldersToExclude_Inner = { "FoldersToExclude", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_FoldersToExclude_MetaData[] = {
		{ "Category", "Folders" },
		{ "Comment", "/**\n\x09* Exclude specified folders from world outliner hierarchy. Child folders are not affected by default.\n\x09* To exclude child folders append /. to the end of folder name\n\x09*/" },
		{ "ModuleRelativePath", "Public/WorldOutliner/HDKEdWorldOutlinerSubsystem.h" },
		{ "ToolTip", "Exclude specified folders from world outliner hierarchy. Child folders are not affected by default.\nTo exclude child folders append /. to the end of folder name" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_FoldersToExclude = { "FoldersToExclude", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHDKEdWorldOutlinerSubsystem, FoldersToExclude), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_FoldersToExclude_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_FoldersToExclude_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoExcludeFolders_MetaData[] = {
		{ "Category", "Folders" },
		{ "Comment", "/** \n\x09* If true, auto exclude specified folders from actors path on drop to the viewport.\n\x09* Folders must be specified one per item without any slashes\n\x09* Append /. to the end of folder name to exclude all child folders\n\x09*/" },
		{ "ModuleRelativePath", "Public/WorldOutliner/HDKEdWorldOutlinerSubsystem.h" },
		{ "ToolTip", "If true, auto exclude specified folders from actors path on drop to the viewport.\nFolders must be specified one per item without any slashes\nAppend /. to the end of folder name to exclude all child folders" },
	};
#endif
	void Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoExcludeFolders_SetBit(void* Obj)
	{
		((UHDKEdWorldOutlinerSubsystem*)Obj)->bAutoExcludeFolders = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoExcludeFolders = { "bAutoExcludeFolders", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UHDKEdWorldOutlinerSubsystem), &Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoExcludeFolders_SetBit, METADATA_PARAMS(Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoExcludeFolders_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoExcludeFolders_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_PrefixesToCut_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_PrefixesToCut,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoCutPrefixes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_SuffixesToCut_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_SuffixesToCut,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoCutSuffixes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoAddIndexes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoCreateFolders,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_PathOverridesByClass_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_PathOverridesByClass_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_PathOverridesByClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_FoldersToExclude_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_FoldersToExclude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::NewProp_bAutoExcludeFolders,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UHDKEdWorldOutlinerSubsystem>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::ClassParams = {
		&UHDKEdWorldOutlinerSubsystem::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::PropPointers),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UHDKEdWorldOutlinerSubsystem, 1804469131);
	template<> HDKEDITORUTILITIES_API UClass* StaticClass<UHDKEdWorldOutlinerSubsystem>()
	{
		return UHDKEdWorldOutlinerSubsystem::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UHDKEdWorldOutlinerSubsystem(Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem, &UHDKEdWorldOutlinerSubsystem::StaticClass, TEXT("/Script/HDKEditorUtilities"), TEXT("UHDKEdWorldOutlinerSubsystem"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UHDKEdWorldOutlinerSubsystem);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
