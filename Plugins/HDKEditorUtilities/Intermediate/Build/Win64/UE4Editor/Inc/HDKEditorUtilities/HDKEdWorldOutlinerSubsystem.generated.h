// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class UObject;
#ifdef HDKEDITORUTILITIES_HDKEdWorldOutlinerSubsystem_generated_h
#error "HDKEdWorldOutlinerSubsystem.generated.h already included, missing '#pragma once' in HDKEdWorldOutlinerSubsystem.h"
#endif
#define HDKEDITORUTILITIES_HDKEdWorldOutlinerSubsystem_generated_h

#define NGKTraining_Plugins_HDKEditorUtilities_Source_HDKEditorUtilities_Public_WorldOutliner_HDKEdWorldOutlinerSubsystem_h_14_SPARSE_DATA
#define NGKTraining_Plugins_HDKEditorUtilities_Source_HDKEditorUtilities_Public_WorldOutliner_HDKEdWorldOutlinerSubsystem_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execUpdateActors); \
	DECLARE_FUNCTION(execUpdateActor); \
	DECLARE_FUNCTION(execOnNewActorsDropped);


#define NGKTraining_Plugins_HDKEditorUtilities_Source_HDKEditorUtilities_Public_WorldOutliner_HDKEdWorldOutlinerSubsystem_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execUpdateActors); \
	DECLARE_FUNCTION(execUpdateActor); \
	DECLARE_FUNCTION(execOnNewActorsDropped);


#define NGKTraining_Plugins_HDKEditorUtilities_Source_HDKEditorUtilities_Public_WorldOutliner_HDKEdWorldOutlinerSubsystem_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHDKEdWorldOutlinerSubsystem(); \
	friend struct Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics; \
public: \
	DECLARE_CLASS(UHDKEdWorldOutlinerSubsystem, UEditorSubsystem, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/HDKEditorUtilities"), NO_API) \
	DECLARE_SERIALIZER(UHDKEdWorldOutlinerSubsystem) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define NGKTraining_Plugins_HDKEditorUtilities_Source_HDKEditorUtilities_Public_WorldOutliner_HDKEdWorldOutlinerSubsystem_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUHDKEdWorldOutlinerSubsystem(); \
	friend struct Z_Construct_UClass_UHDKEdWorldOutlinerSubsystem_Statics; \
public: \
	DECLARE_CLASS(UHDKEdWorldOutlinerSubsystem, UEditorSubsystem, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/HDKEditorUtilities"), NO_API) \
	DECLARE_SERIALIZER(UHDKEdWorldOutlinerSubsystem) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define NGKTraining_Plugins_HDKEditorUtilities_Source_HDKEditorUtilities_Public_WorldOutliner_HDKEdWorldOutlinerSubsystem_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHDKEdWorldOutlinerSubsystem(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHDKEdWorldOutlinerSubsystem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHDKEdWorldOutlinerSubsystem); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHDKEdWorldOutlinerSubsystem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHDKEdWorldOutlinerSubsystem(UHDKEdWorldOutlinerSubsystem&&); \
	NO_API UHDKEdWorldOutlinerSubsystem(const UHDKEdWorldOutlinerSubsystem&); \
public:


#define NGKTraining_Plugins_HDKEditorUtilities_Source_HDKEditorUtilities_Public_WorldOutliner_HDKEdWorldOutlinerSubsystem_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHDKEdWorldOutlinerSubsystem(UHDKEdWorldOutlinerSubsystem&&); \
	NO_API UHDKEdWorldOutlinerSubsystem(const UHDKEdWorldOutlinerSubsystem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHDKEdWorldOutlinerSubsystem); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHDKEdWorldOutlinerSubsystem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UHDKEdWorldOutlinerSubsystem)


#define NGKTraining_Plugins_HDKEditorUtilities_Source_HDKEditorUtilities_Public_WorldOutliner_HDKEdWorldOutlinerSubsystem_h_14_PRIVATE_PROPERTY_OFFSET
#define NGKTraining_Plugins_HDKEditorUtilities_Source_HDKEditorUtilities_Public_WorldOutliner_HDKEdWorldOutlinerSubsystem_h_11_PROLOG
#define NGKTraining_Plugins_HDKEditorUtilities_Source_HDKEditorUtilities_Public_WorldOutliner_HDKEdWorldOutlinerSubsystem_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	NGKTraining_Plugins_HDKEditorUtilities_Source_HDKEditorUtilities_Public_WorldOutliner_HDKEdWorldOutlinerSubsystem_h_14_PRIVATE_PROPERTY_OFFSET \
	NGKTraining_Plugins_HDKEditorUtilities_Source_HDKEditorUtilities_Public_WorldOutliner_HDKEdWorldOutlinerSubsystem_h_14_SPARSE_DATA \
	NGKTraining_Plugins_HDKEditorUtilities_Source_HDKEditorUtilities_Public_WorldOutliner_HDKEdWorldOutlinerSubsystem_h_14_RPC_WRAPPERS \
	NGKTraining_Plugins_HDKEditorUtilities_Source_HDKEditorUtilities_Public_WorldOutliner_HDKEdWorldOutlinerSubsystem_h_14_INCLASS \
	NGKTraining_Plugins_HDKEditorUtilities_Source_HDKEditorUtilities_Public_WorldOutliner_HDKEdWorldOutlinerSubsystem_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define NGKTraining_Plugins_HDKEditorUtilities_Source_HDKEditorUtilities_Public_WorldOutliner_HDKEdWorldOutlinerSubsystem_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	NGKTraining_Plugins_HDKEditorUtilities_Source_HDKEditorUtilities_Public_WorldOutliner_HDKEdWorldOutlinerSubsystem_h_14_PRIVATE_PROPERTY_OFFSET \
	NGKTraining_Plugins_HDKEditorUtilities_Source_HDKEditorUtilities_Public_WorldOutliner_HDKEdWorldOutlinerSubsystem_h_14_SPARSE_DATA \
	NGKTraining_Plugins_HDKEditorUtilities_Source_HDKEditorUtilities_Public_WorldOutliner_HDKEdWorldOutlinerSubsystem_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	NGKTraining_Plugins_HDKEditorUtilities_Source_HDKEditorUtilities_Public_WorldOutliner_HDKEdWorldOutlinerSubsystem_h_14_INCLASS_NO_PURE_DECLS \
	NGKTraining_Plugins_HDKEditorUtilities_Source_HDKEditorUtilities_Public_WorldOutliner_HDKEdWorldOutlinerSubsystem_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HDKEDITORUTILITIES_API UClass* StaticClass<class UHDKEdWorldOutlinerSubsystem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID NGKTraining_Plugins_HDKEditorUtilities_Source_HDKEditorUtilities_Public_WorldOutliner_HDKEdWorldOutlinerSubsystem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
