// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "HDKEditorUtilities.h"
#include "ISettingsModule.h"
#include "ISettingsSection.h"
#include "ISettingsContainer.h"
#include "WorldOutliner/HDKEdWorldOutlinerSubsystem.h"

#define LOCTEXT_NAMESPACE "FHDKEditorUtilitiesModule"

void FHDKEditorUtilitiesModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module

	RegisterSettings();
}

void FHDKEditorUtilitiesModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}

bool FHDKEditorUtilitiesModule::HandleSettingsSaved()
{
	UHDKEdWorldOutlinerSubsystem* WorldOutlinerSubsystem = GetMutableDefault<UHDKEdWorldOutlinerSubsystem>();
	WorldOutlinerSubsystem->SaveConfig();

	return true;
}

void FHDKEditorUtilitiesModule::RegisterSettings()
{
	if (ISettingsModule* SettingsModule = FModuleManager::GetModulePtr<ISettingsModule>("Settings"))
	{
		// Create the new category
		ISettingsContainerPtr SettingsContainer = SettingsModule->GetContainer("Editor");

		SettingsContainer->DescribeCategory("World Outliner Tools",
			LOCTEXT("HDKEdWorldOutlinerCategoryName", "World Outliner Tools"),
			LOCTEXT("HDKEdWorldOutlinerCategoryDescription", "World Outliner extra functions (HDK Editor Utilities)")
		);

		// Register the settings
		ISettingsSectionPtr SettingsSection = SettingsModule->RegisterSettings("Editor", "HDK", "World Outliner Tools",
			LOCTEXT("HDKEdWorldOutlinerSettingsName", "World Outliner Tools"),
			LOCTEXT("HDKEdWorldOutlinerSettingsDescription", "World Outliner extra functions (HDK Editor Utilities)"),
			GetMutableDefault<UHDKEdWorldOutlinerSubsystem>()
		);

		// Register the save handler to your settings, you might want to use it to
		// validate those or just act to settings changes.
		if (SettingsSection.IsValid())
		{
			SettingsSection->OnModified().BindRaw(this, &FHDKEditorUtilitiesModule::HandleSettingsSaved);
		}
	}
}

void FHDKEditorUtilitiesModule::UnregisterSettings()
{
	if (ISettingsModule* SettingsModule = FModuleManager::GetModulePtr<ISettingsModule>("Settings"))
	{
		SettingsModule->UnregisterSettings("Editor", "HDK", "World Outliner Tools");
	}
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FHDKEditorUtilitiesModule, HDKEditorUtilities)