// Copyright 2017-2020 HART Digital, OOO. All Rights Reserved.


#include "WorldOutliner/HDKEdWorldOutlinerSubsystem.h"
#include "Atmosphere/AtmosphericFog.h"
#include "Editor.h"
#include "Editor/EditorEngine.h"
#include "Engine/BlockingVolume.h"
#include "Engine/ReflectionCapture.h"
#include "Engine/SkyLight.h"
#include "Engine/TriggerBase.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/Character.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Misc/MessageDialog.h"
#include "Misc/Paths.h"
#include "Sound/AmbientSound.h"
#include "Sound/AudioVolume.h"
#include "UObject/ConstructorHelpers.h"
#include "HDKEditorUtilitiesTypes.h"


UHDKEdWorldOutlinerSubsystem::UHDKEdWorldOutlinerSubsystem()
	: UEditorSubsystem()
{
	/// Naming
	PrefixesToCut.Add(TEXT("HDKAR"));
	PrefixesToCut.Add(TEXT("HDKEd"));
	PrefixesToCut.Add(TEXT("HDKE"));
	PrefixesToCut.Add(TEXT("HDKUI"));
	PrefixesToCut.Add(TEXT("HDKVR"));
	PrefixesToCut.Add(TEXT("HDK"));
	PrefixesToCut.Add(TEXT("BP_"));
	PrefixesToCut.Add(TEXT("SK_"));
	PrefixesToCut.Add(TEXT("SM_"));

	bAutoCutPrefixes = true;

	SuffixesToCut.Add(TEXT("BP"));
	SuffixesToCut.Add(TEXT("_BP"));
	SuffixesToCut.Add(TEXT("_Decal"));
	SuffixesToCut.Add(TEXT("_Skel"));
	
	bAutoCutSuffixes = true;

	bAutoAddIndexes = true;

	/// Folders
	bAutoCreateFolders = true;

	FoldersToExclude.Add(TEXT("Blueprints/."));
	FoldersToExclude.Add(TEXT("Game"));
	FoldersToExclude.Add(TEXT("HDK"));
	FoldersToExclude.Add(TEXT("HDKAR"));
	FoldersToExclude.Add(TEXT("HDKCore"));
	FoldersToExclude.Add(TEXT("HDKEditor"));
	FoldersToExclude.Add(TEXT("HDKEditorUtilities"));
	FoldersToExclude.Add(TEXT("HDKLibrary"));
	FoldersToExclude.Add(TEXT("HDKUI"));
	FoldersToExclude.Add(TEXT("HDKVR"));
	FoldersToExclude.Add(TEXT("Maps"));
	FoldersToExclude.Add(TEXT("Materials/."));
	FoldersToExclude.Add(TEXT("Meshes/."));
	FoldersToExclude.Add(TEXT("Script"));

	bAutoExcludeFolders = true;
}

void UHDKEdWorldOutlinerSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);

	FEditorDelegates::OnNewActorsDropped.AddUObject(this, &UHDKEdWorldOutlinerSubsystem::OnNewActorsDropped);
}

/// General
void UHDKEdWorldOutlinerSubsystem::OnNewActorsDropped(const TArray<UObject*>& Objects, const TArray<AActor*>& Actors)
{
	for (int32 i = 0; i < Actors.Num(); i++)
	{
		UpdateActorLabel(Actors[i], Objects[i], bAutoCutPrefixes, bAutoCutSuffixes, bAutoAddIndexes);

		if (bAutoCreateFolders)
		{
			UpdateActorFolderPath(Actors[i], Objects[i], bAutoExcludeFolders);
		}
	}
}

void UHDKEdWorldOutlinerSubsystem::GetActorReferenceObjects(AActor* Actor, TArray<UObject*>& OutObjects)
{
	if (!Actor)
	{
		return;
	}

	TArray<UObject*> ActorObjects;
	Actor->GetReferencedContentObjects(ActorObjects);

	// If Blueprint assets should take precedence over any other referenced asset, check if there are any blueprints in this actor's list
	// and if so, add only those.
	if (ActorObjects.ContainsByPredicate([](UObject* Obj) { return Obj->IsA(UBlueprint::StaticClass()); }))
	{
		for (UObject* Object : ActorObjects)
		{
			if (Object->IsA(UBlueprint::StaticClass()))
			{
				OutObjects.Add(Object);
			}
		}
	}
	else
	{
		OutObjects.Append(ActorObjects);
	}
}

void UHDKEdWorldOutlinerSubsystem::UpdateActor(AActor* Actor, bool bCutPrefixes, bool bCutSuffixes, bool bAddIndex, bool bCreateFolders, bool bExcludeFolders)
{
	if (!Actor)
	{
		return;
	}

	TArray<UObject*> ActorObjects;
	GetActorReferenceObjects(Actor, ActorObjects);

	UObject* Object = nullptr;

	if (ActorObjects.Num() != 0)
	{
		Object = ActorObjects[0];
	}

	UKismetSystemLibrary::CreateCopyForUndoBuffer(Actor);

	UpdateActorLabel(Actor, Object, bCutPrefixes, bCutSuffixes, bAddIndex);

	if (bCreateFolders)
	{
		UpdateActorFolderPath(Actor, Object, bExcludeFolders);
	}
}

#define LOCTEXT_NAMESPACE "HDK Ed WOT" 

void UHDKEdWorldOutlinerSubsystem::UpdateActors(const TArray<AActor*>& Actors, bool bCutPrefixes, bool bCutSuffixes, bool bAddIndexes, bool bCreateFolders, bool bExcludeFolders)
{
	if (Actors.Num() > 100)
	{
		FText Message = FText::Format(LOCTEXT("WOT_UpdateActors", "You are about to update {0} actors. Do you want to proceed?"), Actors.Num());

		EAppReturnType::Type DialogResult = FMessageDialog::Open(EAppMsgType::Type::OkCancel, Message, nullptr);
		
		if (DialogResult != EAppReturnType::Type::Ok)
			return;
	}
	
	for (AActor* Actor : Actors)
	{
		UpdateActor(Actor, bCutPrefixes, bCutSuffixes, bAddIndexes, bCreateFolders, bExcludeFolders);
	}
}

/// Naming
void UHDKEdWorldOutlinerSubsystem::UpdateActorLabel(AActor* Actor, UObject* Object, bool bCutPrefixes, bool bCutSuffixes, bool bAddIndex)
{
	check(Actor);

	FString ActorLabel = Actor->GetClass()->GetName();
	if (Object)
	{
		ActorLabel = Object->GetName();
	}

	if (bCutPrefixes)
	{
		CutPrefixes(ActorLabel);
	}

	if (bCutSuffixes)
	{
		CutSuffixes(ActorLabel);
	}

	if (bAddIndex)
	{
		AddIndex(Actor, ActorLabel);
	}

	Actor->SetActorLabel(ActorLabel);
}

void UHDKEdWorldOutlinerSubsystem::CutPrefixes(FString& OutActorLabel)
{
	TArray<FString> Prefixes = PrefixesToCut;
	bool bIsNothingToCut = false;
	while (!bIsNothingToCut)
	{
		bIsNothingToCut = true;
		FString PrefixToCut = TEXT("");
		for (int32 i = Prefixes.Num() - 1; i >= 0; i--)
		{
			const int32 PrefixIndex = OutActorLabel.Find(Prefixes[i], ESearchCase::IgnoreCase, ESearchDir::FromStart);
			if (PrefixIndex == INDEX_NONE)
			{
				Prefixes.RemoveAt(i);
			}
			else if ((PrefixIndex == 0) && (Prefixes[i].Len() >= PrefixToCut.Len()))
			{
				PrefixToCut = Prefixes[i];
				bIsNothingToCut = false;
			}
		}

		if (!bIsNothingToCut)
		{
			OutActorLabel = OutActorLabel.RightChop(PrefixToCut.Len());
		}
	}
}

void UHDKEdWorldOutlinerSubsystem::CutSuffixes(FString& OutActorLabel)
{
	TArray<FString> Suffixes = SuffixesToCut;
	bool bIsNothingToCut = false;
	while (!bIsNothingToCut)
	{
		bIsNothingToCut = true;
		FString SuffixToCut = TEXT("");
		for (int32 i = Suffixes.Num() - 1; i >= 0; i--)
		{
			const int32 SuffixIndex = OutActorLabel.Find(Suffixes[i], ESearchCase::IgnoreCase, ESearchDir::FromEnd);
			if (SuffixIndex == INDEX_NONE)
			{
				Suffixes.RemoveAt(i);
			}
			else if ((SuffixIndex == OutActorLabel.Len() - Suffixes[i].Len()) && (Suffixes[i].Len() >= SuffixToCut.Len()))
			{
				SuffixToCut = Suffixes[i];
				bIsNothingToCut = false;
			}
		}

		if (!bIsNothingToCut)
		{
			OutActorLabel = OutActorLabel.LeftChop(SuffixToCut.Len());
		}
	}
}

void UHDKEdWorldOutlinerSubsystem::AddIndex(AActor* Actor, FString& OutActorLabel)
{
	check(Actor);

	int32 LabelIdx = 1;
	FString Prefix = OutActorLabel;
	FString ModifiedActorLabel = FString::Printf(TEXT("%s_%d"), *OutActorLabel, LabelIdx);

	FCachedActorLabels ActorLabels;

	TSet<AActor*> IgnoreActors;
	IgnoreActors.Add(Actor);
	ActorLabels.Populate(Actor->GetWorld(), IgnoreActors);

	if (ActorLabels.Contains(ModifiedActorLabel))
	{
		// Update the actor label until we find one that doesn't already exist
		while (ActorLabels.Contains(ModifiedActorLabel))
		{
			++LabelIdx;
			ModifiedActorLabel = FString::Printf(TEXT("%s_%d"), *Prefix, LabelIdx);
		}
	}

	OutActorLabel = ModifiedActorLabel;
}

/// Folders
void UHDKEdWorldOutlinerSubsystem::UpdateActorFolderPath(AActor* Actor, UObject* Object, bool bExcludeFolders)
{
	FString Path = Actor->GetPathName();
	FString ActorLabel = Actor->GetClass()->GetName();
	if (Object)
	{
		Path = Object->GetPathName();
		ActorLabel = Object->GetName();
	}

	FString PathOverride;
	for (auto& Elem : PathOverridesByClass)
	{
		if (Actor->GetClass()->IsChildOf(Elem.Key))
		{
			PathOverride = Elem.Value;
			break;
		}
	}

	if (!PathOverride.IsEmpty())
	{
		Path = PathOverride;
		Path = Path.Replace(TEXT("\\"), TEXT("/"));
		Path = Path.Replace(TEXT("|"), TEXT("/"));
	}
	else
	{
		Path = FPaths::GetPath(Path);

		if (bExcludeFolders)
		{
			TArray<FString> PathFolders;
			Path.ParseIntoArray(PathFolders, TEXT("/"));

			FString CollapsedPath;

			for (auto& PathFolder : PathFolders)
			{
				bool bNeedCollapse = false;
				bool bCollapseAll = false;
				for (auto& Folder : FoldersToExclude)
				{
					FString ValidFolder = Folder;
					ValidFolder = ValidFolder.Replace(TEXT("\\"), TEXT("/"));
					ValidFolder = ValidFolder.Replace(TEXT("|"), TEXT("/"));

					int Index = ValidFolder.Find(TEXT("/."), ESearchCase::IgnoreCase, ESearchDir::FromEnd);
					if ((Index != INDEX_NONE) && (Index == ValidFolder.Len() - 2))
					{
						ValidFolder = ValidFolder.LeftChop(2);
						bCollapseAll = true;
					}
					else
					{
						bCollapseAll = false;
					}

					if (PathFolder == ValidFolder)
					{
						bNeedCollapse = true;
						break;
					}
				}

				if (!bNeedCollapse)
				{
					if (!CollapsedPath.IsEmpty())
					{
						CollapsedPath += TEXT("/");
					}
					CollapsedPath += PathFolder;
				}
				else if (bCollapseAll)
				{
					break;
				}
			}

			Path = CollapsedPath;
		}
	}

	if (Path.Len() > 0)
	{
		if (Path.Left(1) == TEXT("/"))
		{
			Path = Path.RightChop(1);
		}
	}

	if (Path.Len() > 0)
	{
		if (Path.Right(1) == TEXT("/"))
		{
			Path = Path.LeftChop(1);
		}
	}

	Actor->SetFolderPath(FName(*Path));
}
