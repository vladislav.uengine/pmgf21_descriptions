// Copyright 2017-2020 HART Digital, OOO. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "EditorSubsystem.h"
#include "HDKEdWorldOutlinerSubsystem.generated.h"


/** Editor subsystem, that correctly rename actors which developer dropped to level and move to paths */
UCLASS(Config = Editor, DefaultConfig, meta = (DisplayName = "World Outliner Subsystem"))
class HDKEDITORUTILITIES_API UHDKEdWorldOutlinerSubsystem : public UEditorSubsystem
{
	GENERATED_BODY()

public:
	UHDKEdWorldOutlinerSubsystem();

	virtual void Initialize(FSubsystemCollectionBase& Collection) override;

	//=====================================================================================
	// General
	//=====================================================================================
protected:
	/**
	* Event called when new actors being dropped to the level
	* @param Objects - Referenced assets
	* @param Actors	- Dropped actors
	*/
	UFUNCTION()
	void OnNewActorsDropped(const TArray<UObject*>& Objects, const TArray<AActor*>& Actors);

	/**
	* Finds reference asset objects to specific actor
	* @param Actor - Specified Actor
	* @param OutObjects - Assets being used by Actor
	*/
	void GetActorReferenceObjects(AActor* Actor, TArray<UObject*>& OutObjects);

public:
	/**
	* Update actor naming and folder path.
	* Actor being named according to their destination and being put to folders according to their location.
	* @param Actor - Actor to organize
	* @param Rename - If true, actor will be renamed
	* @param Index - If true, add index to actor name
	* @param CreateFolders - If true, actor will be put to folders according to their location
	* @param ExcludeFolders - If true, exclude folders from actors path
	*/
	UFUNCTION(BlueprintCallable, Category = "HDK Editor Utilities|World Outliner Subsystem")
	void UpdateActor(AActor* Actor, bool bCutPrefixes = true, bool bCutSuffixes = true, bool bAddIndex = false, bool bCreateFolders = true, bool bExcludeFolders = true);

	/**
	* Update actors naming and folder path.
	* Actors being named according to their destination and being put to folders according to their location.
	* @param Actors - Actors to organize
	* @param Rename - If true, actors will be renamed
	* @param Index - If true, add index to actors name
	* @param CreateFolders - If true, actors will be put to folders according to their location
	* @param ExcludeFolders - If true, collapse folders from actors path
	*/
	UFUNCTION(BlueprintCallable, Category = "HDK Editor Utilities|World Outliner Subsystem")
	void UpdateActors(const TArray<AActor*>& Actors, bool bCutPrefixes = true, bool bCutSuffixes = true, bool bAddIndexes = false, bool bCreateFolders = true, bool bExcludeFolders = true);

	//=====================================================================================
	// Naming
	//=====================================================================================
public:
	/** Prefixes to cut form actors label */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Config, Category = "Naming")
	TArray<FString> PrefixesToCut;

	/** If true, auto cut specified prefixes from actors label on drop to the viewport */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Config, Category = "Naming")
	bool bAutoCutPrefixes;

	/** Suffixes to cut form actors label */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Config, Category = "Naming")
	TArray<FString> SuffixesToCut;

	/** If true, auto cut specified suffixes from actors label on drop to the viewport */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Config, Category = "Naming")
	bool bAutoCutSuffixes;

	/** If true, auto add indexes to actors label on drop to the viewport */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Config, Category = "Naming")
	bool bAutoAddIndexes;

protected:
	/**
	* Update actor's label: cuts prefixes, suffixes and optionally add unique index
	* @param Actor - Update label for specified Actor
	* @param Object - referenced objects
	* @param CutPrefixes - Cut prefixes from label
	* @param CutSuffixes - Cut suffixes from label
	* @param AddIndex - If true, add unique index to label
	*/

	/** If bIsRenamingEnable true, takes Actors and Objects default names according to their paths, cuts prefixes and suffixes and sets new unique Actor label */
	void UpdateActorLabel(AActor* Actor, UObject* Object, bool bCutPrefixes = true, bool bCutSuffixes = true, bool bAddIndex = false);

	/** Cuts prefixes (beginning of the label and splits with underscore) according to user declaration (which user added to PrefixesToCut array) */
	void CutPrefixes(FString& OutActorLabel);

	/** Cuts suffixes (ending of the string and splits with underscore) according to user declaration (which user added to SuffixesToCut array) */
	void CutSuffixes(FString& OutActorLabel);

	/** Adds index */
	void AddIndex(AActor* Actor, FString& OutActorLabel);

	//=====================================================================================
	// Folders
	//=====================================================================================
public:
	/** If true, on actor's dropped to the viewport auto create world outliner folder hierarchy from it's path or path override by class and place actor to that folder */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Config, Category = "Folders")
	bool bAutoCreateFolders;

	/** Override paths for actors by their class */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Config, Category = "Folders")
	TMap<UClass*, FString> PathOverridesByClass;

	/**
	* Exclude specified folders from world outliner hierarchy. Child folders are not affected by default.
	* To exclude child folders append /. to the end of folder name
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Config, Category = "Folders")
	TArray<FString> FoldersToExclude;

	/** 
	* If true, auto exclude specified folders from actors path on drop to the viewport.
	* Folders must be specified one per item without any slashes
	* Append /. to the end of folder name to exclude all child folders
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Config, Category = "Folders")
	bool bAutoExcludeFolders;

protected:
	/** Finds Actor folder path and sets it according to user declaration in source code */
	void UpdateActorFolderPath(AActor* Actor, UObject* Object, bool bExcludeFolders = true);
};
