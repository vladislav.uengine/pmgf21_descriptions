// Copyright 2017-2020 HART Digital, OOO. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
//#include "HDKEditorUtilitiesTypes.generated.h"


DECLARE_LOG_CATEGORY_EXTERN(LogHDKEditorUtilities, Log, All);